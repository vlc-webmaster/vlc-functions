<?php
/*
Plugin Name: VLC Functionality
Plugin URI: https://victorylifechurch.com/
Description: A plugin to add custom post types and taxonomies for Victory Life Website
Version: 1.0
Author: Ronald Fuquay
License: GPLv2
*/


add_action( 'init', 'vlc_register_post_types' );

function vlc_register_post_types() {
	$events_args = array(
		'public' => true,
		'query_var' => 'vlc_event',
		'menu_icon' => 'dashicons-calendar-alt',
		'labels' => array(
			'name' => 'Events',
			'singular_name' => 'Event',
		),
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
		),
	);
	register_post_type( 'vlc_event', $events_args );

	$life_groups_args = array(
		'public' => true,
		'query_var' => 'vlc_life_group',
		'menu_icon' => 'dashicons-groups',
		'labels' => array(
			'name' => 'Life Groups',
			'singular_name' => 'Life Group',
		),
		'rewrite' => array(
			'with_front' => false,
			'slug' => '',
		),
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
		),
	);
	register_post_type( 'vlc_life_group', $life_groups_args );

	$locations_args = array(
		'public' => true,
		'query_var' => 'vlc_location',
		'menu_icon' => 'dashicons-location-alt',
		'labels' => array(
			'name' => 'Locations',
			'singular_name' => 'Location',
		),
		'rewrite' => array(
			'with_front' => false,
			'slug' => 'location',
		),
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
		),
	);
	register_post_type( 'vlc_location', $locations_args );
}


add_action( 'init', 'vlc_register_taxonomies' );

function vlc_register_taxonomies() {
	$type_args = array(
		'query_var'  => 'type',
		'hierarchical' => true,
		'labels'     =>  array(
			'name'           => 'Types',
			'singular_name'  => 'Type',
		),
	);

	register_taxonomy( 'type', 'vlc_life_group', $type_args );
}



?>